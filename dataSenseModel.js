let mongoose = require('mongoose');
// Setup schema

    const dataSenseSchema = new mongoose.Schema({
        temperature: Number,
        gas: Number,
        humidity: Number,
        pressure: Number,
        altitude: Number,
        createdAt: Date
    });
// Export Contact model
module.exports = mongoose.model('DataSense', dataSenseSchema);