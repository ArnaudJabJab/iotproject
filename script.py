# This file has been written to your home directory for convenience. It is
# saved as "/home/pi/bar_graph-2020-04-17-19-35-00.py"

import requests
import time
# Comment this import if you want to use the real sensor data
import random

# Uncomment the whole part to work with your sensor data
### import board
### from busio import I2C
### import adafruit_bme680

# Create library object using our Bus I2C port
# Uncomment these 2 lines to have the real data sensor
# i2c = I2C(board.SCL, board.SDA)
# bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, debug=False)


# change this to match the location's pressure (hPa) at sea level
# Uncomment this line to have the real sensor data
# bme680.sea_level_pressure = 1013.25

url = "http://localhost:8080/api/dataSense"

# comment this line to avoid the fake data
obj = {'temperature': 22, 'humidity': 78, 'pressure': 121, 'gas': 121, 'altitude': 1200}

# Uncomment this line to have the real sensor data
# obj = {'temperature': bme680.temperature, 'humidity': bme680.humidity, 'pressure': bme680.pressure, 'gas': bme680.gas, 'altitude': bme680.altitude}

response = requests.post(url, data=obj)
result = response.json()
sensorId = result['idSensor']
print(sensorId)
time.sleep(5)


# Update our dataSet
def update(id):
    url = "http://localhost:8080/api/dataSense/" + id
    #Comment this line to avoid
    obj = {'temperature': random.randint(0, 101), 'humidity': random.randint(0, 101),
           'pressure': random.randint(0, 101), 'gas': random.randint(0, 101), 'altitude': random.randint(0, 101)}

    # uncomment this line to have realsensor data
    #obj = {'temperature': bme680.temperature, 'humidity': bme680.humidity, 'pressure': bme680.pressure,
     #      'gas': bme680.gas, 'altitude': bme680.altitude}
    response = requests.put(url, data=obj)
    result = response.json()
    print(result)


# This whill update our sensor data every 8 seconds
while True:
    update(sensorId)
    time.sleep(8)
