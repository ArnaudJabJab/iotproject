let express = require('express');
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const app = express();
const port = process.env.PORT || 8080;
let apiRoutes = require("./routes");


app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb://127.0.0.1:27017/IoTProject', {useUnifiedTopology: true , useNewUrlParser: true }, () => {
    console.log('connected to mongodb');
});

app.set('view engine', 'ejs');
app.get('/', (req, res) => {
    res.render('home', { sensorId: process.env['SENSOR_ID'] } );
});

app.use('/api', apiRoutes);

app.listen(port, function () {
    console.log("Running the server Assignment1 on port " + port);
});
