// contactController.js
// Import contact model
const express = require('express');

DataSense = require('./dataSenseModel');
// Handle index actions

exports.index = function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to the IoT Project created by Arnaud Jabiol!'
    });
};

async function getAll() {
    const dataSenses = await DataSense.find();
    return {
        'dataSenses': dataSenses
    }
}
exports.findAll = function (req, res, next) {
    getAll()
        .then(dataSenses => res.json(dataSenses))
        .catch(err => next(err));
};

// Handle view contact info
exports.findOne = function (req, res) {
    DataSense.findById(req.params.id, function (err, dataSense) {
        if (err)
            res.send(err);
        res.json({
            message: 'DataSense details loading..',
            data: dataSense
        });
    });
};
// Handle update dataSense info
exports.update = function (req, res) {
    DataSense.findById(req.params.id, function (err, dataSense) {
        if (err)
            res.send(err);
        dataSense.temperature = req.body.temperature;
        dataSense.humidity = req.body.humidity;
        dataSense.gas = req.body.gas;
        dataSense.pressure = req.body.pressure;
        dataSense.altitude = req.body.altitude;
        dataSense.createAt = req.body.createdAt;
// save the contact and check for errors
        dataSense.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'DataSense Info updated',
                data: dataSense
            });
        });
    });
};

// Handle delete
exports.delete = function (req, res) {
    DataSense.remove({
        _id: req.params.dataSense_id
    }, function (err, dataSense) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'DataSense deleted'
        });
    });
};
