# IoTProject

# Internet of Things course (Beijing Jiaotong University)

Application developed by **Arnaud Jabiol** for BJTU Internet of Things module.

It is an application that permit you to see in real time the data of your sensor in a specific webpage.

### Software and Hardware Requirements

 - Node.js
 - Python
 - MongoDB (mongoose)
 - RaspberryPi
 - Sensor Adafruit BME680

### Installation


Be sure that MongoDb is installed and service is launched

```sh
$ sudo systemctl start mongod.service
```

Install the dependencies and start the server.

```sh
$ cd iotproject
$ npm install -d
$ node index.js 
```
After this you'll get the home page on the localhost address with port **8080**.
Do not forget to start the python script.py
```sh
$ python script.py
```

With both server and python script running, you have the whole project working with the sensor and the data send to our webpage.

![home](./screens/home.png)

### Functionnalities

You can see a table that will show you every 10 seconds the data that your sensor catch.