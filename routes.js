const express = require('express');
let dataController = require('./dataController');
let router = express.Router();
// Set default API response
DataSense = require('./dataSenseModel');

router.get('/dataSense/index', dataController.index);
router.get('/dataSense', dataController.findAll);

router.post('/dataSense', async function(req, res) {
    let status = {
        status: 200,
        idSensor: String,
        message: 'New dataSense created !'
    };
    try {
        let dataSense = new DataSense({
            temperature: req.body.temperature,
            humidity: req.body.humidity,
            gas: req.body.gas,
            pressure: req.body.pressure,
            altitude: req.body.altitude,
            createdAt: Date()
        });
// save the data and check for errors
        dataSense.save();
        status.idSensor = dataSense._id;
        sensorId = dataSense._id;
        process.env['SENSOR_ID'] = dataSense._id;
        console.log(process.env['SENSOR_ID'])
    } catch (error) {
        status.status = 400;
        status.message = error.name + ': ' + error.message
    }
    await res.status(status.status).send(status)
});
router.get('/dataSense/:id', dataController.findOne);
router.put('/dataSense/:id', dataController.update);
router.delete('/dataSense/:id', dataController.delete);


// Export API routes
module.exports = router;